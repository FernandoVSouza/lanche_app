import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { PedidoService } from '../providers/pedido-service/pedido.service';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = TabsPage;

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen,
    private _pedidoService: PedidoService
    ) {
      platform.ready().then(() => {
        statusBar.styleLightContent();
        statusBar.backgroundColorByHexString('#FF0000');
        splashScreen.hide();
      });
  }
}

