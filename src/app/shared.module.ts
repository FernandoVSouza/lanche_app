import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';

import { ComponentsModule } from '../components/components.module';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicModule,
    ComponentsModule,
  ],
  exports: [
    ComponentsModule
  ]
})

export class SharedModule { }
