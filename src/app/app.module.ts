import { NgModule, ErrorHandler } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { MyApp } from './app.component';
import { LOCALE_ID } from '@angular/core';
import localePt from '@angular/common/locales/pt';
import { BrMasker4Module } from 'brmasker4';
import { Geolocation } from '@ionic-native/geolocation';
import { HaversineService } from "ng2-haversine";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SharedModule } from './shared.module';

import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { CardapioPage } from '../pages/cardapio/cardapio';
import { PedidosPage } from '../pages/pedidos/pedidos';
import { MinhaContaPage } from '../pages/minha-conta/minha-conta';
import { LoginPage } from '../pages/login/login';
import { PedidosDetalhesPage } from '../pages/pedidos/pedidos-detalhes/pedidos-detalhes';
import { ItemDetalhesPage } from '../pages/pedidos/pedidos-detalhes/item-detalhes/item-detalhes';
import { NotificacaoPage } from '../pages/minha-conta/notificacao/notificacao';

import { OpcoesCarrinhoComponent } from '../components/opcoes-carrinho/opcoes-carrinho';

import { CardapioService } from '../providers/cardapio/cardapio.service';
import { UrlService } from '../providers/url/url.service';
import { AuthProvider } from '../providers/auth/auth.service';
import { StorageProvider } from '../providers/storage/storage.service';
import { LoadingService } from '../providers/loading-service/loading-service';
import { GeoService } from '../providers/geo-service/geo-service.service';
import { EntregaService } from '../providers/entrega-service/entrega-service.service';
import { PedidoService } from '../providers/pedido-service/pedido.service';
import { DadosPage } from '../pages/minha-conta/dados/dados';

registerLocaleData(localePt, 'pt-BR');

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CardapioPage,
    PedidosPage,
    MinhaContaPage,
    LoginPage,
    PedidosDetalhesPage,
    ItemDetalhesPage,
    TabsPage,
    OpcoesCarrinhoComponent,
    NotificacaoPage,
    DadosPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    SharedModule,
    IonicModule.forRoot(MyApp, {
      mode: 'md'
    }),
    IonicStorageModule.forRoot({
      name: '_AppLanche',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    BrMasker4Module
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CardapioPage,
    PedidosPage,
    MinhaContaPage,
    LoginPage,
    PedidosDetalhesPage,
    ItemDetalhesPage,
    TabsPage,
    OpcoesCarrinhoComponent,
    NotificacaoPage,
    DadosPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    CardapioService,
    UrlService,
    AuthProvider,
    StorageProvider,
    LoadingService,
    GeoService,
    EntregaService,
    PedidoService,
    Geolocation,
    HaversineService
  ]
})
export class AppModule {}
