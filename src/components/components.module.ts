import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { CategoriasComponent } from './categorias/categorias';

export const components = [
		CategoriasComponent,
  ];

@NgModule({
	declarations: [components],
	imports: [IonicModule],
	exports: [components]
})
export class ComponentsModule {}
