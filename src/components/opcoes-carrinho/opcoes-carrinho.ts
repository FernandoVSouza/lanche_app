import { Component } from '@angular/core';
import { ViewController, NavParams, ModalController } from 'ionic-angular';

@Component({
  template: `
  <ion-list>
    <button ion-item (click)="btnEditarClick()">Editar</button>
    <button ion-item (click)="btnRemoverClick()">Remover</button>
  </ion-list>`
})
export class OpcoesCarrinhoComponent {
  private _pedido: any;

  constructor(
    private _viewCtrl: ViewController,
    private _navParams: NavParams,
    private _modalController: ModalController
  ) {}

  ionViewDidLoad() {
    this._pedido = this._navParams.get('pedido');
    if(!this._pedido) this._viewCtrl.dismiss();
  }

  btnRemoverClick() {
    this._viewCtrl.dismiss('remover');
  }

  btnEditarClick(): void {
    this._openModal();
  }

  private _openModal(): void {
    let modal = this._modalController.create("LancheDetalhesPage", {
      pedido: this._pedido
    });
    modal.present();
    modal.onDidDismiss(() => {
      this._viewCtrl.dismiss();
    });
  }
}