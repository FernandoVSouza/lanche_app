import { Component } from '@angular/core';
import { ModalController, Modal  } from 'ionic-angular';

import { CardapioService } from '../../providers/cardapio/cardapio.service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private _detalhesModal: Modal;
  produtos: any[];
  showSpinner: boolean = true;

  constructor(
    private _cardapioService: CardapioService,
    private _modalController: ModalController
  ) { }

  ionViewDidLoad() {
    this._getDestaques();
  }

  btnDetalhes(produto: any): void {
    this._detalhesModal = this._modalController.create("LancheDetalhesPage", { produto: produto });
    this._detalhesModal.present();
    this._detalhesModal.onDidDismiss(() => {});
  }

  private _getDestaques(): void {
    this._cardapioService.getDestaquesFromWeb()
    .subscribe(result => {
      this.produtos = result;
      this._formataIngredientes();
      this.showSpinner = false;
    }, error => {
      console.error(error);
      this.showSpinner = false;
    });
  }

  private _formataIngredientes(): void {
    this.produtos.forEach(p => {
      p.ingredientes = p.ingredientes.join(', ');
    });
  }
}