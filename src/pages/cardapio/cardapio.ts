import { Component } from '@angular/core';
import { ModalController, Modal  } from 'ionic-angular';

import { CardapioService } from '../../providers/cardapio/cardapio.service';

@Component({
  selector: 'page-cardapio',
  templateUrl: 'cardapio.html',
})
export class CardapioPage {

  private _detalhesModal: Modal;
  items: any[];
  showSpinner: boolean = true;
  
  constructor(
    private _cardapioService: CardapioService,
    private _modalController: ModalController
  ) {}

  //INTERFACES METHODS

  ionViewDidLoad() {
    this._cardapioService.getCardapioFromWeb()
      .subscribe(result => {
        this.items = result;
        this._formataIngredientes();
        this.showSpinner = false;
        this._cardapioService.produtos = this.items;
      }, error => {
        console.error(error);
        this.showSpinner = false;
      });
  }

  //EVENTS

  btnDetalhes(produto: any): void {
    this._configModalDetalhes(produto);
    this._detalhesModal.present();
    this._detalhesModal.onDidDismiss(() => {});
  }

  //PRIVATES METHODS

  private _configModalDetalhes(produto): void {
    this._detalhesModal = this._modalController.create("LancheDetalhesPage", { produto: produto });
  }

  private _formataIngredientes(): void {
    this.items.forEach(c => {
      c.produtos.forEach(p => {
        p.ingredientes = p.ingredientes.join(', ');
      });
    });
  }

}