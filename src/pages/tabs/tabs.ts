import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { CardapioPage } from '../cardapio/cardapio';
import { PedidosPage } from '../pedidos/pedidos';
import { MinhaContaPage } from '../minha-conta/minha-conta';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab0Root = HomePage;
  tab1Root = CardapioPage;
  tab2Root = PedidosPage;
  tab3Root = MinhaContaPage;

  constructor() { }
}