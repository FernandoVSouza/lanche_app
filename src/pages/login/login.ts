import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';

import { AuthProvider } from '../../providers/auth/auth.service';
import { StorageProvider } from '../../providers/storage/storage.service';
import { LoadingService } from '../../providers/loading-service/loading-service';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  tela: string = 'login';
  usuarioLogin = { email: null, password: null };
  usuarioCadastro = { name: null, email: null, password: null, c_password: null, phone: null, tipo: 'app' };

  constructor(
    private _authProvider: AuthProvider,
    private _storageService: StorageProvider,
    private _viewCtrl: ViewController,
    private _loading: LoadingService) {
  }

  ionViewDidLoad() {}

  btnChangeTela(tela: string) {
    this.tela = tela;
  }

  btnCloseModal(): void {
    this._viewCtrl.dismiss(false);
  }

  //Login Methods
  authenticateUsuario(): void {
    const loading = this._loading.loadingController('Entrando, aguarde...');
    loading.present()
    try {
      this._verificaUsuarioLogin();
      this._authProvider.postAuthUsuario(this.usuarioLogin)
        .subscribe(
          ret => {
            loading.dismiss();
            this._storageService.removeCarrinho();
            this._storageService.saveUsuario(ret);
            this._viewCtrl.dismiss(true);
          },
          error => {
            loading.dismiss();
            this._loading.toastController(`Erro: ${error.error}`);
            console.error(`Erro: ${error}`);
            throw error;
          }
        );
    } catch (error) {
      loading.dismiss();
    }
  }

  //Cadastro Methods

  cadastrarUsuario(): void {
    const loading = this._loading.loadingController('Cadastrando, aguarde...');
    loading.present();
    try {
      this._verificaUsuarioCadastro();
      this._authProvider.postRegisterUsuario(this.usuarioCadastro)
        .subscribe(
          retorno => {
            loading.dismiss();
            this.tela = 'login';
            this._limpaUsuarioCadastrado();
          },
          error => {
            loading.dismiss();
          }
        )
    } catch (error) {
      loading.dismiss();
      console.error(`Erro: ${error}`);
    }
  }

  //Login Private Methods
  private _verificaUsuarioLogin(): void {
    if(!this.usuarioLogin.email && !this.usuarioLogin.password) {
      throw 'Informar todos os campos!';
    }
  }

  private _verificaUsuarioCadastro(): void {
    if(!this.usuarioCadastro.name) {
      throw 'Informar o nome!';
    } else if(!this.usuarioCadastro.email) {
      throw 'Informar o E-Mail!';
    } else if(!this.usuarioCadastro.password) {
      throw 'Informar o senha!';
    } else if(!this.usuarioCadastro.c_password) {
      throw 'Informar a confirmação da senha!';
    } else if(this.usuarioCadastro.password != this.usuarioCadastro.c_password) {
      throw 'Senhas não correspondem';
    }
  }

  private _limpaUsuarioCadastrado() {
    this.usuarioCadastro.name = null;
    this.usuarioCadastro.email = null;
    this.usuarioCadastro.password = null;
    this.usuarioCadastro.c_password = null;
    this.usuarioCadastro.phone = null;
  }

}
