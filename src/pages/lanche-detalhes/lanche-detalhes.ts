import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController, ModalController, Modal } from 'ionic-angular';

import { CardapioService } from '../../providers/cardapio/cardapio.service';
import { PedidoService } from '../../providers/pedido-service/pedido.service';
import { LoadingService } from '../../providers/loading-service/loading-service';
import { StorageProvider } from '../../providers/storage/storage.service';

@IonicPage()
@Component({
  selector: 'page-lanche-detalhes',
  templateUrl: 'lanche-detalhes.html',
})
export class LancheDetalhesPage {

  produto: any;
  pedido: any;
  qtde: number = 0;
  incrementos: any;
  produtoPedido: any;
  showIncremento: boolean = false;
  private _observacaoModal: Modal;

  constructor(
    private _navParams: NavParams,
    private _viewCtrl: ViewController,
    private _cardapioService: CardapioService,
    private _modalController: ModalController,
    private _pedidoService: PedidoService,
    private _loadingService: LoadingService,
    private _storageService: StorageProvider
    ) {
    }

  ionViewDidLoad() {
    this._init();
  }

  closeModal(): void {
    this._viewCtrl.dismiss();
  }

  AddIncremento(e: any, incremento: any): void {
    if(e.value) {
      this.produtoPedido.precoTotal = Number.parseFloat(this.produtoPedido.precoTotal) + Number.parseFloat(incremento.preco);
      this.produtoPedido.idIncrementos.push(incremento.id_ingrediente);
      incremento.selected = true;
    } else {
      this.produtoPedido.precoTotal = Number.parseFloat(this.produtoPedido.precoTotal) - Number.parseFloat(incremento.preco);
      const incrementos = this.produtoPedido.idIncrementos.filter(i => i != incremento.id_ingrediente);
      this.produtoPedido.idIncrementos = incrementos;
      incremento.selected = false;
    }
  }

  async AddCarrinho() {
    try {
      let ret = await this._getUserAuthenticated();

      if(ret) {
        this._pedidoService.addPedido(this.produtoPedido);
        this._loadingService.toastController('Produto Adicionado na sacola!');
        this._viewCtrl.dismiss();
      } else {
        this._loadingService.toastController('Ops! É preciso estar logado para realizar o pedidos.');
      }
    } catch (error) {
      this._loadingService.toastController(error);
    }
  }

  btnModalObservacao(): void {
    this._observacaoModal = this._modalController.create("ObservacaoPage", {
      observacao: this.produtoPedido.observacao
    });
    this._observacaoModal.onDidDismiss(data => {
      if(data) this.produtoPedido.observacao = data;
    });
    this._observacaoModal.present();
  }

  btnShowIncremento(): void {
    this.showIncremento = !this.showIncremento;
  }

  btnAddQtde(): void {
    this.produtoPedido.qtdeProduto += 1;
  }

  btnRemoveQtde(): void {
    if(this.produtoPedido.qtdeProduto != 1) this.produtoPedido.qtdeProduto -= 1;
  }

  cortarLanche(e: any): void {
    if(e.value) this.produtoPedido.cortarNoMeio = true;
    else this.produtoPedido.cortarNoMeio = false;
  }

  private _getCardapioFromWeb(): void {
    this._cardapioService.getIncrementosFromWeb()
      .subscribe(retorno => {
        retorno.map(r => r.selected = false);
        this.incrementos = retorno;
        this._populateIncremento();
      }, error => {
        console.error(error);
        this._loadingService.toastController('Ops! Não foi possível carregar os acrescimos!');
      });
  }

  private _gerarIDLocal(): number {
    let data = new Date();
    return data.getTime();
  }

  private _init(): void {
    this.produto = this._navParams.get('produto');
    this.pedido = this._navParams.get('pedido');
    this._getCardapioFromWeb();

    if(this.produto) {
      this.produtoPedido = {
        idLocal: this._gerarIDLocal(),
        idProduto: this.produto.id_produto,
        nomeProduto: this.produto.descricao,
        idIncrementos: [],
        preco: Number.parseFloat(this.produto.preco_unitario),
        precoTotal: Number.parseFloat(this.produto.preco_unitario),
        observacao: '',
        qtdeProduto: 1,
        cortarNoMeio: false,
        produto: this.produto
      };
    } else if(this.pedido) {
      this.produtoPedido = this.pedido;
      this.produto = this.pedido.produto;
    }
  }

  private _populateIncremento(): void {
    this.produtoPedido.idIncrementos.forEach(i => {

      this.incrementos.forEach(ing => {
        if(ing.id_ingrediente == i) ing.selected = true;
      });
    });
  }

  private async _getUserAuthenticated() {
    const user: any = await this._storageService.getUsuarioAuthenticated();
    if(!user) {
      return false;
    } else {
      return true;
    }
  }
}
