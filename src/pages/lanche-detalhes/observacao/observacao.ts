import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-observacao',
  templateUrl: 'observacao.html',
})
export class ObservacaoPage {
  observacao: string = '';

  constructor(
    private _viewCtrl: ViewController,
    private _navParams: NavParams
  ) {}

  ionViewDidLoad() {
    let obs = this._navParams.get('observacao');
    if(obs) this.observacao = obs;
  }

  closeModal(): void {
    this._viewCtrl.dismiss();
  }

  btnSalvar(): void {
    this._viewCtrl.dismiss(this.observacao);
  }
}