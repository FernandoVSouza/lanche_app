import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LancheDetalhesPage } from './lanche-detalhes';

@NgModule({
  declarations: [
    LancheDetalhesPage,
  ],
  imports: [
    IonicPageModule.forChild(LancheDetalhesPage),
  ],
})
export class LancheDetalhesPageModule {}
