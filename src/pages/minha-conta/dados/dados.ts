import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';

import { StorageProvider } from '../../../providers/storage/storage.service';
import { AuthProvider } from '../../../providers/auth/auth.service';
import { LoadingService } from '../../../providers/loading-service/loading-service';

@Component({
  selector: 'page-dados',
  templateUrl: 'dados.html',
})
export class DadosPage {
  user: any = null;
  carregando: boolean = false;

  constructor(
    private _viewCtrl: ViewController,
    private _storage: StorageProvider,
    private _authService: AuthProvider,
    private _loadingService: LoadingService
  ) {}

  ionViewDidLoad() {
    this._init();
  }

  btnCloseModal(): void {
    this._viewCtrl.dismiss();
  }

  saveUpdateUsuario(): void {
    try {
      this.carregando = true;

      if(this.user) {
        if(!this.validateEmail(this.user.user.email)) {
          this._loadingService.toastController('E-Mail inválido!');
        } else if(this.user.user.password != this.user.user.c_password) {
          this._loadingService.toastController('Senhas não correspondem!');
        } else {
          this._authService.putUpdateUsuario(this.user.user)
            .subscribe(
              () => {
                this.carregando = false;
                this.user.user.password = null;
                this.user.user.c_password = null;
                this._storage.saveUsuario(this.user);
                this._loadingService.toastController('Salvo com sucesso!');
              },
              error => {
                throw error
              }
            );
        }
      }
    } catch (error) {
      this.carregando = false;
      this._loadingService.toastController('Erro ao tentar salvar. Tente novamente!');
    }
  }

  private _init(): void {
    this._getUserAuthenticated(true);
  }

  private async _getUserAuthenticated(retorno) {
    if(retorno) {
      const user: any = await this._storage.getUsuarioAuthenticated();
      if(user) {
        this.user = user;
      }
    }
  }

  private validateEmail(email): boolean {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

}
