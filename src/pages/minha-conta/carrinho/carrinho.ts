import { Component } from '@angular/core';
import { IonicPage, ViewController, Modal, ModalController, ActionSheetController, PopoverController } from 'ionic-angular';

import { PedidoService } from '../../../providers/pedido-service/pedido.service';
import { StorageProvider } from '../../../providers/storage/storage.service';
import { LoadingService } from '../../../providers/loading-service/loading-service';
import { OpcoesCarrinhoComponent } from '../../../components/opcoes-carrinho/opcoes-carrinho';

@IonicPage()
@Component({
  selector: 'page-carrinho',
  templateUrl: 'carrinho.html',
})
export class CarrinhoPage {
  private _modal: Modal;
  formaPagamento: any;
  valorTotal: number = 0;
  carregando: boolean = false;

  carrinho: any = {
    idCliente: null,
    pedidos: [],
    endereco: null,
    enderecoGps: null,
    tipoPagamento: null,
    valorPagar: 0,
    valorTotal: 0,
    valorTroco: 0
  };
  
  constructor(
    private _viewCtrl: ViewController,
    private _pedidoService: PedidoService,
    private _modalCtrl: ModalController,
    private _actionSheetCtrl: ActionSheetController,
    private _storageService: StorageProvider,
    private _loadingService: LoadingService,
    private _popoverCtrl: PopoverController
  ) {}

  ionViewDidLoad() {
    try {
      this._getCarrinho();      
    } catch (error) {
      this._loadingService.toastController('Erro ao carregar pedido!');
      console.error(error);
    }
  }

  btnCloseModal(): void {
    this._viewCtrl.dismiss();
  }

  changeEnderecoEntrega(): void {
    this._modal = this._modalCtrl.create('EntregaPage');
    this._modal.present();
    this._modal.onDidDismiss(retorno => {
      if(retorno) this._getCarrinho();
    });
  }

  btnTrocoConfirmacao(): void {
    let op = false;
    const actionSheet = this._actionSheetCtrl.create({
      title: 'Vai precisar de troco?',
      buttons: [
        {
          text: 'Sim',
          handler: () => {
            this._openModalTroco();
            op = true;
          }
        },{
          text: 'Não',
          handler: () => {
            this.formaPagamento = null;
            op = true;
          }
        },{
          text: 'Cancelar',
          handler: () => {
            this.formaPagamento = null;
            op = true;
          }
        }
      ]
    });
    actionSheet.present();
    actionSheet.onDidDismiss(data => {
      if(!op) {
        this.formaPagamento = null;
      }
    })
  }

  btnCartaoClick(): void {
    this.carrinho.tipoPagamento = this.formaPagamento;
    this.carrinho.valorPagar = null;
    this.carrinho.valorTroco = null;
    this._pedidoService.saveCarrinho();
  }

  btnConfirmarClick(): void {
    try {
      this.carrinho.valorTotal = this.valorTotal;
      this.carrinho.valorTroco =  this.carrinho.valorPagar - this.valorTotal;
      this._verificaDadosDoPedido();
      this._sendPedidoToWeb(this.carrinho);
    } catch (error) {
      this._loadingService.toastController('Erro: ' + error);
    }
  }

  btnOpcoesClick(pedido: any): void {
    this._presentOpcoes(pedido);
  }

  private _getCarrinho(): void {
    this.carregando = true;
    this._pedidoService.getCarrinho()
    .subscribe(
      ret => {
        this.carrinho = ret;
        this.valorTotal = 0;
        this.carrinho.pedidos.forEach(p => {
          this.valorTotal += (p.precoTotal * p.qtdeProduto);
        });
        this._init();
        this.carregando = false;
      },
      error => {
        this.carregando = false;
          throw error;
        }
      );
  }

  private _openModalTroco(): void {
    this._modal = this._modalCtrl.create('TrocoPage', { valorTotal: this.valorTotal });
    this._modal.present();
    this._modal.onDidDismiss(valorPagar => {
      if(valorPagar > this.valorTotal) {
        this.carrinho.tipoPagamento = this.formaPagamento;
        this.carrinho.valorPagar = valorPagar;
        this._pedidoService.saveCarrinho();
      } else {
        this.formaPagamento = null;
      }
    });
  }

  private async _init() {
    const user = await this._storageService.getUsuarioAuthenticated();

    if(this.carrinho.tipoPagamento) this.formaPagamento = this.carrinho.tipoPagamento; 
    if(user) {
      this.carrinho.idCliente = user.user.id;
      this._pedidoService.saveCarrinho();
    } else {
      this._loadingService.toastController('Por favor, entre com sua conta ou cadastre-se para poder ter acesso a esta tela!');
      this._viewCtrl.dismiss();
    }
  }

  private _sendPedidoToWeb(pedido: any): void {
    this._pedidoService.postSavePedido(pedido)
      .subscribe(
        () => {
          this._loadingService.toastController('Pedido Realizado com sucesso!');
          this._pedidoService.removeCarrinho();
          this._viewCtrl.dismiss();
        },
        error => {
          this._loadingService.toastController('Erro ao realizar o pedido!');
        }
      );
  }

  private _verificaDadosDoPedido(): void {
    if(!this.carrinho.tipoPagamento) {
      throw 'Informar o tipo de Pagamento!';
    } else if (!this.carrinho.endereco && !this.carrinho.enderecoGps) {
      throw 'Informar o endereço de entrega!';
    }
  }

  private _presentOpcoes(pedido: any): void {
    let popover = this._popoverCtrl.create(OpcoesCarrinhoComponent, { pedido: pedido });
    popover.present();
    popover.onDidDismiss((data) => {
      if(data == 'remover') {
        this._removerOp(pedido);
      }
      this._getCarrinho();
    });
  }

  private _removerOp(pedido: any): void {
    let retorno = this.carrinho.pedidos.findIndex(p => p.idLocal == pedido.idLocal)
    if(retorno != -1) {
      this.carrinho.pedidos.splice (retorno, 1);
      if(!this.carrinho.pedidos.length) {
        this._pedidoService.removeCarrinho();
        this._viewCtrl.dismiss();
      }
    }
  }
}