import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-troco',
  templateUrl: 'troco.html',
})
export class TrocoPage {
  valorTotal: number = null;
  valorPagar: number = null;
  mensagemErro: string = '';

  constructor(
    private _navParams: NavParams,
    private _viewCtrl: ViewController
  ) {}

  ionViewDidLoad() {
    this._init();
  }
  
  btnCloseModal(): void {
    this._viewCtrl.dismiss(0);
  }

  btnConfirmarClick(): void {
    this.valorPagar = this.valorPagar;

    if(this.valorPagar > this.valorTotal) {
      this._viewCtrl.dismiss(this.valorPagar);
    } else {
      this.mensagemErro = 'O valor informado é menor que o valor Total.';
    }
  }

  private _init(): void {
    const valorTotal = this._navParams.get('valorTotal');
    this.valorTotal = valorTotal;
  }
}