import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrocoPage } from './troco';
import { BrMasker4Module } from 'brmasker4';

@NgModule({
  declarations: [
    TrocoPage,
  ],
  imports: [
    IonicPageModule.forChild(TrocoPage),
    BrMasker4Module
  ],
})
export class TrocoPageModule {}
