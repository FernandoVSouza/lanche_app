import { Component } from '@angular/core';
import { IonicPage, ViewController } from 'ionic-angular';

import { GeoService } from '../../../../providers/geo-service/geo-service.service';
import { StorageProvider } from '../../../../providers/storage/storage.service';
import { EntregaService } from '../../../../providers/entrega-service/entrega-service.service';
import { LoadingService } from '../../../../providers/loading-service/loading-service';

@IonicPage()
@Component({
  selector: 'page-busca-por-cep',
  templateUrl: 'busca-por-cep.html',
})
export class BuscaPorCepPage {
  cepEncontrado: boolean = false;
  buscando: boolean = false;
  cep: string = null;
  endereco: any = {
    fk_user: null,
    numero: null,
    rua: null,
    bairro: null,
    complemento: null,
    cep: null,
    cidade: null,
    uf: null,
    lat: null,
    lng: null
  };
  private _user: any;

  constructor(
    private _geoService: GeoService,
    private _storageService: StorageProvider,
    private _viewCtrl: ViewController,
    private _entregaService: EntregaService,
    private _loadingService: LoadingService
    ) {}

  ionViewDidLoad() {
    try {
      this._getUserAuthenticated();
    } catch (error) {
      console.error(`Erro: ${error}`);
      this._loadingService.toastController(`Erro: ${error}`);
    }
  }

  buscarCEPClick(cep: string): void {
    this.buscando = true;
    try {
      if(cep) {
        this._geoService.getEnderecoByCEP(cep)
        .subscribe(
          ret => {
            if(ret) {
              this.cepEncontrado = true;
              this._formataEnderecos(ret);
              this._populaEndereco(ret);
            } else {
              throw 'CEP não encontrado!';
            }
          },
          error => {
            throw error;
          }
          );
        } else {
          throw 'Informar o cep!';
      }
    } catch (error) {
      this.buscando = false;
      console.error(`Erro: ${error}`);
      this._loadingService.toastController(`Erro: ${error}`);
    }
  }

  salvarEnderecoClick(): void {
    try {
      this._verificaEndereco();

      this._entregaService.saveEntrega(this.endereco)
        .subscribe(
          ret => {
            this._viewCtrl.dismiss(true);
          },
          error => {
            throw error;
          }
        );
    } catch (error) {
      console.error(`Erro: ${error}`);
      this._loadingService.toastController(`Erro: ${error}`);
    }
  }

  btnCloseModal(): void {
    this._viewCtrl.dismiss(false);
  }

  private _formataEnderecos(dados: any) {
    try {
      let rua: string = this._formaTexto(dados.logradouro);
      let bairro: string = this._formaTexto(dados.bairro);
      let cidade: string = this._formaTexto(dados.localidade);
      let uf: string = this._formaTexto(dados.uf);
      const parametro: string = `${rua},+${bairro},+${cidade},+${uf}`;
      this._getLatitudeLongitude(parametro);
    } catch (error) {
      throw error;      
    }
  }

  private _formaTexto(texto: string): string {
    return texto.split(" ").join("+");
  }

  private _getLatitudeLongitude(parametro: string): void {
    this._geoService.getLatitudeLongitude(parametro)
      .subscribe(
        ret => {
          this.endereco.lat = ret.results[0].geometry.location.lat;
          this.endereco.lng = ret.results[0].geometry.location.lng;
          this.buscando = false;
        },
        error => {
          throw error;
        }
      );
  }

  private _populaEndereco(dados: any): void {
    this.endereco.fk_user = this._user.id;
    this.endereco.rua = dados.logradouro;
    this.endereco.bairro = dados.bairro;
    this.endereco.complemento = dados.complemento;
    this.endereco.cep = dados.cep;
    this.endereco.cidade = dados.localidade;
    this.endereco.uf = dados.uf;
  }

  private async _getUserAuthenticated() {
    const user = await this._storageService.getUsuarioAuthenticated();
    this._user = user.user;
  }

  private _verificaEndereco(): void {
      if(!this.endereco.numero) throw 'Informar o número!';
  }

}