import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuscaPorCepPage } from './busca-por-cep';
import { BrMasker4Module } from 'brmasker4';

@NgModule({
  declarations: [
    BuscaPorCepPage,
  ],
  imports: [
    IonicPageModule.forChild(BuscaPorCepPage),
    BrMasker4Module
  ],
})
export class BuscaPorCepPageModule {}
