import { Component } from '@angular/core';
import { IonicPage, ViewController, Modal, ModalController, } from 'ionic-angular';
import { HaversineService } from "ng2-haversine";

import { EntregaService } from '../../../providers/entrega-service/entrega-service.service';
import { StorageProvider } from '../../../providers/storage/storage.service';
import { GeoService } from '../../../providers/geo-service/geo-service.service';
import { LoadingService } from '../../../providers/loading-service/loading-service';
import { PedidoService } from '../../../providers/pedido-service/pedido.service';

@IonicPage()
@Component({
  selector: 'page-entrega',
  templateUrl: 'entrega.html',
})
export class EntregaPage {
  private _modal: Modal;
  private _user: any = null;
  private _localDoLanche = { latitude: -22.1573956, longitude: -51.4521557 };
  private _localDoUsuario = { latitude: 0, longitude: 0 };
  carregando: boolean = false;
  entregas: any[];
  entregaSelected: any;
  enderecoPeloGps: string;
  
  constructor(
    private _viewCtrl: ViewController,
    private _modalCtrl: ModalController,
    private _entregaService: EntregaService,
    private _storageService: StorageProvider,
    private _geoService: GeoService,
    private _haversineService: HaversineService,
    private _loadingService: LoadingService,
    private _pedidoService: PedidoService
    ) {}

  async ionViewDidLoad() {
    try {
      await this._getUserAuthenticated();
      this._getEnderecosByUserAuthenticted();
    } catch (error) {
      this._loadingService.toastController(`Erro: ${error}`);
    }
  }

  btnCloseModal(): void {
    this._viewCtrl.dismiss();
  }

  btnBuscaPorCEPClick(): void {
    this._modal = this._modalCtrl.create('BuscaPorCepPage');
    this._modal.present()
    this._modal.onDidDismiss(retorno => {
      if(retorno) {
        this._getEnderecosByUserAuthenticted();
      }
    });
  }

  btnLocalizacaoAtual(): void {
    try {
      this.carregando = true;
      this._geoService.getLocation()
        .subscribe(
          ret => {
            this._getEnderecoByLatLng(ret);
          },
          error => {
            console.error(error);
            this.carregando = false;
            this._loadingService.toastController(`Erro: ${error}`);
          }
        );
    } catch (error) {
      this._loadingService.toastController(`Erro: ${error}`);
    }
  }

  btnContinuarClick(): void {
    try {
      let distancia = null;
      if(this.entregaSelected == 0) {
        distancia = this._haversineService.getDistanceInKilometers(this._localDoLanche, this._localDoUsuario);
      } else {
        distancia = this._haversineService.getDistanceInKilometers(this._localDoLanche, { latitude: this.entregaSelected.latitude, longitude: this.entregaSelected.longitude });
      }
  
      if(distancia > 5) {
        throw 'Desculpe não atendemos nessa localização!';
      } else {
        if(this.entregaSelected == 0) {
          this._pedidoService.addEnderecoGPS(this.enderecoPeloGps);
          this._viewCtrl.dismiss(true);
        } else {
          this._pedidoService.addEndereco(this.entregaSelected);
          this._viewCtrl.dismiss(true);
        }
      }
    } catch (error) {
      this._loadingService.toastController(`Erro: ${error}`);      
    }
  }

  private _getEnderecosByUserAuthenticted(): void {
    try {
      this._entregaService.getEnderecosByUserAuthenticted(this._user.id)
        .subscribe(
          ret => {
            this.entregas = ret;
          },
          error => {
            throw error;
          }
        )
    } catch (error) {
      console.error(`Erro: ${error}`);
      throw error;
    }
  }

  private async _getUserAuthenticated() {
    const user = await this._storageService.getUsuarioAuthenticated();
    if(user) this._user = user.user;
  }

  private _getEnderecoByLatLng({ latitude, longitude}): void {
    this._localDoUsuario = { latitude, longitude};

    this._geoService.getEnderecoByLatLngGoogle(latitude, longitude)
      .subscribe(
        retorno => {
          this.enderecoPeloGps = retorno.results[0].formatted_address;
          this.carregando = false;
        },
        error => {
          console.error(error);
          this.carregando = false;
          throw error;
        }
      );
  }
}