import { Component } from '@angular/core';
import { Modal, ModalController, AlertController } from 'ionic-angular';

import { LoginPage } from '../login/login';
import { DadosPage } from './dados/dados';
import { NotificacaoPage } from './notificacao/notificacao';
import { StorageProvider } from '../../providers/storage/storage.service';
import { PedidoService } from '../../providers/pedido-service/pedido.service';

@Component({
  selector: 'page-minha-conta',
  templateUrl: 'minha-conta.html',
})
export class MinhaContaPage {
  private _modal: Modal;
  user: any = null;
  carrinho: any;
  
  constructor(
    private _modalCtrl: ModalController,
    private _storage: StorageProvider,
    private _alertCtrl: AlertController,
    private _pedidoService: PedidoService
  ) {}

  ionViewDidLoad() {
    this._init();
  }

  openModalLogin(): void {
    try {
      this._configModalLogin();
      this._modal.present()
    } catch (error) {
      console.error(`Erro: ${error}`);
    }
  }

  btnSairClick(): void {
    this._alertController();
  }

  btnEnderecoClick(): void {
    this._configModalEndereco();
    this._modal.present();
  }

  btnCarrinhoClick(): void {
    this._modal = this._modalCtrl.create('CarrinhoPage');
    this._modal.present();
    this._modal.onDidDismiss(retorno => {
      this._init();
    });
  }

  btnNotificacaoClick(): void {
    this._modal = this._modalCtrl.create(NotificacaoPage);
    this._modal.present();
    this._modal.onDidDismiss(retorno => {});
  }

  btnDadosClick(): void {
    this._modal = this._modalCtrl.create(DadosPage);
    this._modal.present();
    this._modal.onDidDismiss(retorno => {});
  }

  //PRIVATE METHODS

  private _configModalEndereco(): void {
    this._modal = this._modalCtrl.create('EntregaPage');
    this._modal.onDidDismiss(retorno => {});
  }

  private _configModalLogin(): void {
    this._modal = this._modalCtrl.create(LoginPage);
    this._modal.onDidDismiss(retorno => {
      this._getUserAuthenticated(retorno);
    });
  }

  private _init(): void {
    this._getUserAuthenticated(true);
    this._pedidoService.getCarrinho()
    .subscribe(
      ret => {
        this.carrinho = ret;
      },
      error => {
        throw error;
      }
    );
  }

  private async _getUserAuthenticated(retorno) {
    if(retorno) {
      const user: any = await this._storage.getUsuarioAuthenticated();
      if(user) this.user = user;
    }
  }

  private _alertController(): void {
    const alert = this._alertCtrl.create({
      title: 'Deseja realmente sair?',
      message: 'Para poder tem acesso as suas informações pessoais será necessário logar novamente!',
      buttons: [
        {
          text: 'Sim',
          handler: () =>  this._logOffUsuario()
        },
        {
          text: 'Não',
          handler: () => {}
        }
      ]
    });
    alert.present();
  }

  private _logOffUsuario() {
    this._storage.removeUsuario();
    this._storage.removeCarrinho();
    this.user = null;
  }
}