import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';

import { StorageProvider } from '../../../providers/storage/storage.service';
import { AuthProvider } from '../../../providers/auth/auth.service';
import { LoadingService } from '../../../providers/loading-service/loading-service';

@Component({
  selector: 'page-notificacao',
  templateUrl: 'notificacao.html',
})
export class NotificacaoPage {
  user: any = null;
  carregando: boolean = false;

  constructor(
    private _viewCtrl: ViewController,
    private _storage: StorageProvider,
    private _authService: AuthProvider,
    private _loadingService: LoadingService
  ) {}

  ionViewDidLoad() {
    this._init();
  }

  btnCloseModal(): void {
    this._viewCtrl.dismiss();
  }

  btnSaveNotificacaoClick(): void {
    try {
      this.carregando = true;
      if(this.user) {
        this._authService.putUpdateUsuarioNotificacao(this.user.user)
        .subscribe(
          () => {
            this.carregando = false;
            this._storage.saveUsuario(this.user);
            this._loadingService.toastController('Salvo com sucesso!');
          },
          error => {
            throw error
          }
        );
      }
    } catch (error) {
      this.carregando = false;
      this._loadingService.toastController('Erro ao tentar salvar. Tente novamente!');
    }
  }

  private _init(): void {
    this._getUserAuthenticated(true);
  }

  private async _getUserAuthenticated(retorno) {
    if(retorno) {
      const user: any = await this._storage.getUsuarioAuthenticated();
      if(user) {
        this.user = user;
      }
    }
  }
}