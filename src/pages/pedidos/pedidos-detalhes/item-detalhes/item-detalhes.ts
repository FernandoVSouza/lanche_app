import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-item-detalhes',
  templateUrl: 'item-detalhes.html',
})
export class ItemDetalhesPage {
  produto: any;

  constructor(
    private _viewCtrl: ViewController,
    private _navParams: NavParams
  ){}

  ionViewDidLoad() {
    this._init();
  }

  btnCloseModal(): void {
    this._viewCtrl.dismiss();
  }

  private async _init() {
    const produto = await this._navParams.get('produto');
    this.produto = produto ? produto : null;
  }
}