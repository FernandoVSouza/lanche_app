import { Component } from '@angular/core';
import { NavParams, ModalController } from 'ionic-angular';

import { ItemDetalhesPage } from './item-detalhes/item-detalhes';

@Component({
  selector: 'page-pedidos-detalhes',
  templateUrl: 'pedidos-detalhes.html',
})
export class PedidosDetalhesPage {
  pedido: any;

  constructor(
    private _modalCtrl: ModalController,
    private _navParams: NavParams
  ) {}

  ionViewDidLoad() {
    this._init();
  }

  itemDetalhes(item: any): void {
    const modal = this._modalCtrl.create(ItemDetalhesPage, { produto: item });
    modal.present();
    modal.onDidDismiss(data => {});
  }

  private async _init() {
    const pedido = await this._navParams.get('pedido');
    this.pedido = pedido ? pedido : null;
  }
}