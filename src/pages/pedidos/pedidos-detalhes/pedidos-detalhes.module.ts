import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PedidosDetalhesPage } from './pedidos-detalhes';

@NgModule({
  declarations: [
    PedidosDetalhesPage,
  ],
  imports: [
    IonicPageModule.forChild(PedidosDetalhesPage),
  ],
})
export class PedidosDetalhesPageModule {}
