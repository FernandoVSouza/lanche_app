import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { StorageProvider } from '../../providers/storage/storage.service';
import { PedidoService } from '../../providers/pedido-service/pedido.service';
import { PedidosDetalhesPage } from './pedidos-detalhes/pedidos-detalhes';

@Component({
  selector: 'page-pedidos',
  templateUrl: 'pedidos.html',
})
export class PedidosPage {
  private _user: any;
  pedidos: any[];
  showSpinner: boolean = true;
  messageError: boolean = true;

  constructor(
    private _storageService: StorageProvider,
    private _pedidoService: PedidoService,
    private _navCtrl: NavController
  ) {
  }

  async ionViewDidEnter() {
    await this._init();
    await this._getPedidos(null);
  }

  btnDetalhesClick(pedido: any): void {
    this._navCtrl.push(PedidosDetalhesPage, { pedido: pedido });
  }

  refreshEvent(refresher): void {
    this._getPedidos(refresher);
  }

  private async _init() {
    const user_access: any = await this._storageService.getUsuarioAuthenticated();
    this._user = user_access ? user_access.user : null;
  }

  private _getPedidos(refresher?: any): void {
    if(this._user && this._user.id) {
      this._pedidoService.getPedidosRealizadosFromWeb(this._user.id)
        .subscribe(
          retorno => {
            this.pedidos = retorno;
            this.showSpinner = false;
            this.messageError = false;
            if(refresher) refresher.complete();
          },
          error => {
            console.error('Erro: ', error);
            this.showSpinner = false;
            if(refresher) refresher.complete();
          }
        );
    } else {
      this.showSpinner = false;
      if(refresher) refresher.complete();
    }
  }
}
