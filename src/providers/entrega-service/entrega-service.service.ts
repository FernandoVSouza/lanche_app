import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { UrlService } from '../url/url.service';
import { Observable } from 'rxjs';
import { StorageProvider } from '../storage/storage.service';

@Injectable()
export class EntregaService {

  private _token: string = '';
  private _token_type: string = '';
  private _header: any = null;

  constructor(
    private _http: HttpClient,
    private _urlService: UrlService,
    private _storageService: StorageProvider
    ) {}

  saveEntrega(entrega: any): Observable<any> {
    return this._http.post(this._urlService.getURLEntrega(), entrega);
  }

  getEnderecosByUserAuthenticted(fk_user: any): Observable<any> {
    return this._http.get(this._urlService.getURLGetEntregaByUser() + fk_user);
  }
}