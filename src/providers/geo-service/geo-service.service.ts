import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';
import { Geolocation } from '@ionic-native/geolocation';

@Injectable()
export class GeoService {
  private readonly _keyGoogleGeo: string = 'AIzaSyD10mX9loSWKLtDzL217jyPZ-qnNGdfbPQ';

  constructor(
    private _http: HttpClient,
    private geo: Geolocation
     ) {}

  getEnderecoByCEP(cep: string): Observable<any> {
    return this._http.get(`https://viacep.com.br/ws/${cep}/json`);
  }

  getLatitudeLongitude(parametro: string): Observable<any>{
    return this._http.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${parametro}&key=${this._keyGoogleGeo}`);
  }
  
  getEnderecoByLatLng(lat: number, lng: number): Observable<any> {
    return this._http.get(`https://nominatim.openstreetmap.org/reverse?format=json&lat=${lat}&lon=${lng}`);
  }

  getEnderecoByLatLngGoogle(lat: number, lng: number): Observable<any> {
    return this._http.get(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${this._keyGoogleGeo}`);
  }

  getLocation(): Observable<any> {
    return Observable.create(async (observer: Observer<any>) => {
      try {
        const data = await this.geo.getCurrentPosition();
        observer.next(data.coords);
      } catch (error) {
        observer.error(error)
      }
    });
  }
}
