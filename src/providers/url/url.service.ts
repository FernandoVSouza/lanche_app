import { Injectable } from '@angular/core';

@Injectable()
export class UrlService{
  private _URL: string;
  private _URLMOV: string;

  constructor() {
    this._URL = 'http://localhost:8000/api/mobile';
    this._URLMOV = 'http://localhost:8000/api/movimentacao';
    // this._URLMOV = 'http://fernandovicente.alphi.media/api/movimentacao';
    // this._URL = 'http://fernandovicente.alphi.media/api/mobile';
  }

  getURLCardapio(): string {
    return `${this._URL}/cardapio`;
  }

  getURLDestaques(): string {
    return `${this._URL}/getProdutoDestaques`;
  }

  getURLIncrementos(): string {
    return `${this._URL}/getIncrementos`;
  }

  getURLAuthUsuario(): string {
    return `${this._URL}/loginApp`;
  }

  getURLCadastroApp(): string {
    return `${this._URL}/registerApp`;
  }

  getURLUpdateUsuarioNotificacao(): string {
    return `${this._URL}/updateUserNotificacao/`;
  }

  getURLUpdateUsuario(): string {
    return `${this._URL}/updateUser/`;
  }

  getURLEntrega(): string {
    return `${this._URL}/entrega`;
  }

  getURLDeletaEntrega(): string {
    return `${this._URL}/entrega/deletaEntrega/`;
  }

  getURLGetEntregaByUser(): string {
    return `${this._URL}/entrega/getEntregaByUser/`;
  }

  getURLResoucePedido(): string {
    return `${this._URLMOV}/pedido/`;
  }

  getURLSavePedido(): string {
    return `${this._URLMOV}/savePedido/`;
  }

  getURLResouceProduto(): string {
    return `${this._URL}/produto/`;
  }

  getURLPedidosByCliente(): string {
    return `${this._URLMOV}/pedido/getPedidosByCliente/`;
  }
}
