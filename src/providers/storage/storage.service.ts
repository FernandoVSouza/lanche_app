import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class StorageProvider {
  private _URL: string = 'appLanche';

  constructor(
    private _storage: Storage
  ) {}

  saveUsuario(usuario: any): void {
    this._storage.set(`${this._URL}@user`, usuario);
  }

  async getUsuarioAuthenticated() {
    const dataAtual = new Date();
    const user = await this._storage.get(`${this._URL}@user`);
    if(user) {
      if(dataAtual < new Date(user.expires_at))
        return user
      else
        return null;
    } else {
      return null;
    }
  }

  removeUsuario(): void {
    this._storage.remove(`${this._URL}@user`);
  }
  
  saveCarrinho(pedido: any): void {
    this._storage.set(`${this._URL}@carrinho`, pedido);
  }
  
  async getCarrinho() {
    const carrinho = await this._storage.get(`${this._URL}@carrinho`);
    return carrinho ? carrinho : null;
  }

  removeCarrinho(): void {
    this._storage.remove(`${this._URL}@carrinho`);
  }
}