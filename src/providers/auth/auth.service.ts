import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { UrlService } from '../url/url.service';
import { Observable } from 'rxjs';

@Injectable()
export class AuthProvider {

  constructor(
    private _http: HttpClient,
    private _urlService: UrlService) {}


  postAuthUsuario(usuario: any): Observable<any> {
    const url = this._urlService.getURLAuthUsuario();
    return this._http.post(url, usuario);
  }

  postRegisterUsuario(usuario: any): Observable<any> {
    const url = this._urlService.getURLCadastroApp();
    return this._http.post(url, usuario);
  }

  putUpdateUsuarioNotificacao(usuario: any): Observable<any> {
    const url = this._urlService.getURLUpdateUsuarioNotificacao();
    const body = {
      notificacao: usuario.notificacao,
      notificacao_email: usuario.notificacao_email,
      notificacao_sms: usuario.notificacao_sms,
      notificacao_whatsapp: usuario.notificacao_whatsapp,
    };

    return this._http.put(url + usuario.id, body);
  }

  putUpdateUsuario(usuario: any): Observable<any> {
    const url = this._urlService.getURLUpdateUsuario();
    return this._http.put(url + usuario.id, usuario);
  }
}
