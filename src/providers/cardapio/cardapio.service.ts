import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';

import { UrlService } from '../url/url.service';

@Injectable()
export class CardapioService {
  produtos: any[];

  constructor(
    private _http: HttpClient,
    private _urlService: UrlService
    ) {}

    getProdutosJaBaixados(): Observable<any> {
      return Observable.create(async (observer: Observer<any>) => {
        try {
          observer.next(this.produtos);
        } catch (error) {
          observer.error(error)
        }
      });
    }

    getCardapioFromWeb(): Observable<any> {
      return this._http.get(this._urlService.getURLCardapio());
    }

    getDestaquesFromWeb(): Observable<any> {
      return this._http.get(this._urlService.getURLDestaques());
    }

    getIncrementosFromWeb(): Observable<any> {
      return this._http.get(this._urlService.getURLIncrementos());
    }
}