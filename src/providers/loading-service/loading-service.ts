import { Injectable } from '@angular/core';
import { Loading, LoadingController, ToastController, Toast  } from 'ionic-angular';

@Injectable()
export class LoadingService {

  constructor(
    private _loadingCtrl: LoadingController,
    private _toastCtrl: ToastController
  ) {}

  loadingController(texto: string): Loading {
    return this._loadingCtrl.create({
      content: texto
    });
  }

  toastController(texto: string): void {
    this._toastCtrl.create({
      message: texto,
      duration: 1500,
      position: 'top'
    }).present();
  }
}
