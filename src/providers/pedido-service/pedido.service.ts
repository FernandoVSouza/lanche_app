import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs/Rx';

import { StorageProvider } from '../storage/storage.service';
import { UrlService } from '../url/url.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PedidoService {
  private _carrinho: any = {
    pedidos: [],
    endereco: null,
    enderecoGps: '',
    tipoPagamento: '',
    valorPagar: 0,
    valorTotal: 0,
    valorTroco: 0
  }

  constructor(
    private _storageService: StorageProvider,
    private _urlService: UrlService,
    private _http: HttpClient,
  ) {
    this._init();
  }

  getCarrinho(): Observable<any> {
    return Observable.create(async (observer: Observer<any>) => {
      try {
        observer.next(this._carrinho);
      } catch (error) {
        observer.error(error)
      }
    });
  }

  addEndereco(endereco: any): void {
    this._carrinho.endereco = endereco;
    this._carrinho.enderecoGps = '';
    this._storageService.saveCarrinho(this._carrinho);
  }

  addEnderecoGPS(enderecoGps: string): void {
    this._carrinho.enderecoGps = enderecoGps;
    this._carrinho.endereco = null;
    this._storageService.saveCarrinho(this._carrinho);
  }

  addPedido(pedido: any): void {
    let item = this._carrinho.pedidos.find(p => p.idLocal == pedido.idLocal);
    if(item) {
      item = pedido;
      this._storageService.saveCarrinho(this._carrinho);
    } else {
      this._carrinho.pedidos.push(pedido);
      this._storageService.saveCarrinho(this._carrinho);
    }
  }

  saveCarrinho(): void {
    this._storageService.saveCarrinho(this._carrinho);
  }

  removeCarrinho(): void {
    this._storageService.removeCarrinho();
    this._init();
  }

  private async _init() {
    const carrinho = await this._storageService.getCarrinho();

    if(carrinho) {
      this._carrinho = carrinho;
    } else {
      this._carrinho = {
        pedidos: [],
        endereco: null,
        enderecoGps: '',
        tipoPagamento: '',
        valorPagar: 0,
        valorTotal: 0,
        valorTroco: 0
      }
    }
  }

  //------- HTTP METHODS --------- //
  
  postSavePedido(pedido: any): Observable<any> {
    return this._http.post(this._urlService.getURLSavePedido(), pedido);
  }

  getProduto(id_produto: number): Observable<any> {
    return this._http.get(`${this._urlService.getURLResouceProduto()}${id_produto}`);
  }

  getPedidosRealizadosFromWeb(id_cliente: number): Observable<any> {
    return this._http.get(`${this._urlService.getURLPedidosByCliente()}${id_cliente}`);
  }
}